﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSVParsing
{
    public partial class Form1 : Form
    {

        private List<string> spam = new List<string>();
        private List<string> ham = new List<string>();

        private List<string> stemSpam = new List<string>();
        private List<string> stemHam = new List<string>();

        private Analyzer analyzer = new Analyzer();

        private EnglishPorter2Stemmer stem = new EnglishPorter2Stemmer();

        public Form1()
        {
            InitializeComponent();
            chart1.Series.Clear();
            chart1.Series.Add("Spam");
            chart1.Series.Add("Ham");
            chart1.ChartAreas[0].AxisX.Interval = 10;

            chart2.Series.Clear();
            chart2.Series.Add("Spam");
            chart2.Series.Add("Ham");
            chart2.ChartAreas[0].AxisX.Interval = 50;
        }

        public void ParseCSV(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(new[] { ',' }, 2);
                    
                    if (values[0].Equals("v1"))
                        continue;
                    if (values[0].Equals("spam"))
                    {   
                        string myString = new String(values[1].Where(c => Char.IsLetter(c) || Char.IsWhiteSpace(c)).ToArray());
                        string afterStem = StopwordTool.RemoveStopwords(myString);
                        afterStem = afterStem.ToLower();
                        afterStem = prepareForStemming(afterStem, 0);
                        spam.Add(afterStem);
                    }
                    else
                    {
                        string myString = new String(values[1].Where(c => Char.IsLetter(c) || Char.IsWhiteSpace(c)).ToArray());
                        string afterStem = StopwordTool.RemoveStopwords(myString);
                        afterStem = afterStem.ToLower();
                        afterStem = prepareForStemming(afterStem, 1);
                        ham.Add(afterStem);
                    }

                }
            }
        }

        public string prepareForStemming(string myString, int i)
        {
            string[] stemString = myString.Split(' ');
            string afterStem = "";
            foreach (String s in stemString)
            {
                if (!s.Equals(" ") && !s.Equals(""))
                {
                    string s1 = stem.Stem(s).Value;
                    if(i == 0)
                    {
                        analyzer.AddSpamDict(s1);
                    }
                    else
                        analyzer.AddHamDict(s1);
                    afterStem = afterStem + " " + string.Join(" ", s1);
                }
            }
            return afterStem;
        }

        public void getString()
        {
            int i = 1;
            foreach (String str in spam)
            {
                Console.WriteLine(i + " " + str);
                i++;
            }

            foreach (String str in ham)
            {
                Console.WriteLine(i + " " + str);
                i++;
            }
        }

        public void getStemString()
        {
            int i = 1;
            foreach (String str in stemSpam)
            {
                Console.WriteLine(i + " " + str);
                i++;
            }

            foreach (String str in stemHam)
            {
                Console.WriteLine(i + " " + str);
                i++;
            }
        }

        private void prepareForAnalyzis()
        {
            foreach (string s in spam)
            {
                string[] str = s.Split(' ');
                foreach(string word in str)
                {
                    if(!(word.Equals("") || (word.Equals(" "))))
                        this.analyzer.AddSpam(word);
                }
            }

            foreach (string s in ham)
            {
                string[] str = s.Split(' ');
                foreach (string word in str)
                {
                    if (!(word.Equals("") || (word.Equals(" "))))
                        this.analyzer.AddHam(word);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ParseCSV("./sms-spam-corpus.csv");
            prepareForAnalyzis();
            analyzer.AnalyzeSpamWords();
            analyzer.AnalyzeHamWords();
           foreach (string s in spam)
             analyzer.AnalyzeSpamSentences(s);

            foreach (string s in ham)
                analyzer.AnalyzeHamSentences(s);

            for (int i = 0; i<analyzer.getSpam().Length; i++)
                chart1.Series["Spam"].Points.AddXY(i, analyzer.getSpam()[i]);
            for (int i = 0; i < analyzer.getHam().Length; i++)
                chart1.Series["Ham"].Points.AddXY(i, analyzer.getHam()[i]);

            for (int i = 0; i < analyzer.getSpamSent().Length; i++)
                chart2.Series["Spam"].Points.AddXY(i, analyzer.getSpamSent()[i]);
            for (int i = 0; i < analyzer.getHamSent().Length; i++)
                chart2.Series["Ham"].Points.AddXY(i, analyzer.getHamSent()[i]);

               foreach(KeyValuePair<string,int> s in analyzer.getTopSpam())
               {
                     richTextBox1.Text += s.Key + ": " + s.Value + "\n";
               }


            foreach (KeyValuePair<string, int> s in analyzer.getTopHam())
            {
                richTextBox2.Text += s.Key + ": " + s.Value + "\n";
            }

            label4.Text += analyzer.CountSpamWordsAverage().ToString();
            label6.Text += analyzer.CountHamWordsAverage().ToString();

            label5.Text += analyzer.CountSpamSentAverage().ToString();
            label7.Text += analyzer.CountHamSentAverage().ToString();
            //getString();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = this.textBox1.Text;
            string type = this.BayesComboBox.Text;
            string category = this.analyzer.BayesClassify(type,s);
            this.label3.Text = category;
        }
    }
}
