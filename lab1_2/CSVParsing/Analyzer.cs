﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace CSVParsing
{
    class Analyzer
    {
        private List<string> spam = new List<string>();
        private List<string> ham = new List<string>();
        private int[] arrSpam = new int[50];
        private int[] arrHam = new int[50];

        private int[] arrSpamSent = new int[500];
        private int[] arrHamSent = new int[500];

        private Dictionary<string, int> Spamwords = new Dictionary<string, int>();
        private Dictionary<string, int> Hamwords = new Dictionary<string, int>();

        private Dictionary<string, int> unregisteredSpam = new Dictionary<string, int>();
        private Dictionary<string, int> unregisteredHam = new Dictionary<string, int>();

        public Analyzer()
        {
        }

        public void AddSpam(string s)
        {
            this.spam.Add(s);           
        }

        public void AddHam(string s)
        {
            this.ham.Add(s);
        }

        public void AddSpamDict(string s)
        {
            if (!this.Spamwords.ContainsKey(s))
            {
                this.Spamwords.Add(s, 1);
            }
            else
            {
                this.Spamwords[s] += 1;               
            }
        }


        public void AddHamDict(string s)
        {
            if (!this.Hamwords.ContainsKey(s))
            {
                this.Hamwords.Add(s, 1);               
            }
            else
            {
                this.Hamwords[s] += 1;
            }
        }


        public KeyValuePair<string, int>[] getTopSpam()
        {
            KeyValuePair<string, int>[] arr = new KeyValuePair<string, int>[20];
            var myList = this.Spamwords.ToList();
            myList.Sort((pair2, pair1) => pair1.Value.CompareTo(pair2.Value));

            int i = 0;
            foreach (KeyValuePair<string, int> s in myList)
            {
                if (i >= 20)
                    break;
                arr[i] = s;
                i++;
            }
            return arr;
        }

        public KeyValuePair<string, int>[] getTopHam()
        {
            KeyValuePair<string, int>[] arr = new KeyValuePair<string, int>[20];
            var myList = this.Hamwords.ToList();
            myList.Sort((pair2, pair1) => pair1.Value.CompareTo(pair2.Value));

            int i = 0;
            foreach (KeyValuePair<string, int> s in myList)
            {
                if (i >= 20)
                    break;
                arr[i] = s;
                i++;
            }
            return arr;
        }


        public void AnalyzeSpamWords()
        {
            foreach (string s in spam)
            {
                arrSpam[s.Length] += 1;
            }         
        }

        public void AnalyzeHamWords()
        {        
            foreach (string s in ham)
            {
                arrHam[s.Length] += 1;
            }         
        }


        public void AnalyzeSpamSentences(string s)
        {
            this.arrSpamSent[s.Length] += 1;
        }

        public void AnalyzeHamSentences(string s)
        {
            this.arrHamSent[s.Length] += 1;
        }


        public double CountSpamSentAverage()
        {
            double N = 0;
            double length = 0;
            for (int i = 0; i < arrSpamSent.Length; i++)
            {
                N += arrSpamSent[i];
                if (arrSpamSent[i] != 0 && !arrSpamSent[i].Equals(null))
                    length += i * arrSpamSent[i];
            }
            double res = length / N;
            return res;
        }


        public double CountHamSentAverage()
        {
            double N = 0;
            double length = 0;
            for (int i = 0; i < arrHamSent.Length; i++)
            {
                N += arrHamSent[i];
                if (arrHamSent[i] != 0 && !arrHamSent[i].Equals(null))
                    length += i * arrHamSent[i];
            }
            double res = length / N;
            return res;
        }
        


        public double CountSpamWordsAverage()
        {
            double N = 0;
            double length = 0;
            for(int i = 0; i<arrSpam.Length; i++)
            {
                N += arrSpam[i];
                if (arrSpam[i] != 0 && !arrSpam[i].Equals(null))
                    length += i * arrSpam[i];
            }           
            double res = length / N;
            return res;
        }

        public double CountHamWordsAverage()
        {
            double N = 0;
            double length = 0;
            for (int i = 0; i < arrHam.Length; i++)
            {
                N += arrHam[i];
                if (arrHam[i] != 0 && !arrHam[i].Equals(null))
                    length += i * arrHam[i];
            }
            double res = length / N;
            return res;
        }

        public int[] getSpam()
        {
            return this.arrSpam;
        }

        public int[] getHam()
        {
            return this.arrHam;
        }

        public int[] getSpamSent()
        {
            return this.arrSpamSent;
        }

        public int[] getHamSent()
        {
            return this.arrHamSent;
        }

        private double countBayesPType(string type)
        {
            double spamNum = 0.0;
            double hamNum = 0.0;
            for (int i = 0; i < arrSpamSent.Length; i++)
            {
                spamNum += arrSpamSent[i];
            }

            for (int i = 0; i < arrHamSent.Length; i++)
            {
                hamNum += arrHamSent[i];
            }

            switch (type)
            {
                case "spam":
                    return spamNum / (spamNum + hamNum);
                case "ham":
                    return hamNum / (spamNum + hamNum);
                default:
                    return 1.0;                   
            }
            
        }

        private double countBayesPBodyTextType(string type,string[] sent)
        {
            double P = 0.0;
            double Pword = 0.0;
            double all = 0.0;
            switch (type)
            {
                case "spam":
                    foreach (KeyValuePair<string, int> s in this.Spamwords)
                    {
                        all += s.Value;                       
                    }
                    foreach (string s in sent)
                    {
                        bool isInSpam = true;
                        if (!this.Spamwords.ContainsKey(s))
                        {
                            if (this.unregisteredSpam.ContainsKey(s))
                            {
                                this.unregisteredSpam[s] += 1;
                            }
                            else
                            {
                                this.unregisteredSpam.Add(s, 1);
                               
                            }
                            isInSpam = false;
                        }
                        if (isInSpam)
                        {
                            Pword = this.Spamwords[s];
                            double thisP = Math.Log(Pword / all);
                            P +=thisP;
                            Console.WriteLine(s + " " + Pword + " " + thisP + "  " + P);
                        }
                        else
                        {
                            if (this.Hamwords.ContainsKey(s))
                                Pword = this.Hamwords[s] + 1;
                            else
                                Pword = 1;
                            all = 0;
                             foreach (KeyValuePair<string, int> hams in this.Hamwords)
                            {
                                all += hams.Value;
                            }
                            all = all + this.unregisteredSpam[s] + this.unregisteredSpam.Count();
                            double thisP = Math.Log(Pword / all);
                            P += thisP;
                        }
                    }
                    break;
                case "ham":
                    foreach (KeyValuePair<string, int> s in this.Hamwords)
                    {
                        all += s.Value;
                    }
                    foreach (string s in sent)
                    {
                        bool isInHam = true;
                        if (!this.Hamwords.ContainsKey(s))
                        {
                            if (this.unregisteredHam.ContainsKey(s))
                            {
                                this.unregisteredHam[s] += 1;
                            }
                            else
                            {
                                this.unregisteredHam.Add(s, 1);                             
                            }
                            isInHam = false;
                        }
                        if (isInHam)
                        {
                            Pword = this.Hamwords[s];
                            double thisP = Math.Log(Pword / all);
                            P += thisP;
                            Console.WriteLine("ham: " + s + " " + Pword + " " + thisP + "  " + P);
                        }
                        else
                        {
                            if (this.Spamwords.ContainsKey(s))
                                Pword = this.Spamwords[s] + 1;
                            else
                                Pword = 1;
                            all = 0;
                            foreach (KeyValuePair<string, int> spams in this.Spamwords)
                            {
                                all += spams.Value;
                            }
                            all = all + this.unregisteredHam[s] + this.unregisteredHam.Count();
                            double thisP = Math.Log(Pword / all);
                            P += thisP;
                        }
                    }
                    break;
                default:
                    Console.WriteLine("default");
                    return 0;
            }               
            return P;
        }

        public string BayesClassify(string type,string sent)
        {
            bool isInSpam = true;
            bool isInHam = true;
            string[] TestString = sent.Split(' ');

            double PSpam = 0.0;
            double PSpamBodytext = 0.0;
            double PBodytextSpam = 0.0;

            double PHam = 0.0;
            double PHamBodytext = 0.0;
            double PBodytextHam = 0.0;

            //SPAM 
            PSpam = countBayesPType("spam");
            PBodytextSpam = countBayesPBodyTextType("spam", TestString);
            Console.WriteLine("PBodytextSpam" +PBodytextSpam);
            Console.WriteLine("PSpam " + PSpam);
            PSpamBodytext = Math.Log(PSpam) + PBodytextSpam;            
            //HAM
            PHam = countBayesPType("ham");
            PBodytextHam = countBayesPBodyTextType("ham", TestString);
            Console.WriteLine("PBodytextHam" + PBodytextHam);
            Console.WriteLine("PHam " + PHam);
            PHamBodytext = Math.Log(PHam) + PBodytextHam;
            

            Console.WriteLine(PSpamBodytext + "   " + PHamBodytext);

            if (PSpamBodytext > PHamBodytext)
                return "spam";
            else
                return "ham";
            
        }
    }
}
