﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace clasters
{
    public partial class Form1 : Form
    {
        private ClasterAnalyzer clasters;
        private int k;
        private List<Point>[] clasters_arr;
        private List<Point> centroid_list;

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        public Form1()
        {
            InitializeComponent();

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            ComboboxItem item = new ComboboxItem();
            item.Text = "birch 1";
            item.Value = "birch1.txt";
            comboBox1.Items.Add(item);

            ComboboxItem item1 = new ComboboxItem();
            item1.Text = "birch 2";
            item1.Value = "birch2.txt";
            comboBox1.Items.Add(item1);

            ComboboxItem item2 = new ComboboxItem();
            item2.Text = "birch 3";
            item2.Value = "birch3.txt";
            comboBox1.Items.Add(item2);

            ComboboxItem item3 = new ComboboxItem();
            item3.Text = "s 1";
            item3.Value = "s1.txt";
            comboBox1.Items.Add(item3);

            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            prepareData();
            label3.Text = "Processing...";
            clasters.count();
            if (backgroundWorker1.IsBusy != true)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void prepareData()
        {
            k = int.Parse(textBox1.Text);
            string s = "./points datasets/" + (comboBox1.SelectedItem as ComboboxItem).Value.ToString();
            clasters = new ClasterAnalyzer(s, k);
            clasters_arr = new List<Point>[k];           
        }

        private void createChart1()
        {
            chart1.Series.Clear();
            for (int i = 0; i < k; i++)
            {
                List<int> x = new List<int>();
                List<int> y = new List<int>();
                foreach (Point p in clasters_arr[i])
                {
                    x.Add(p.X);
                    y.Add(p.Y);
                }
                chart1.Series.Add("claster " + (i+1).ToString());
                chart1.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                chart1.Series[i].IsVisibleInLegend = false;
                chart1.Series[i].Points.DataBindXY(x, y);
            }   
        }

        private void createChart2()
        {
            chart2.Series.Clear();
            for (int i = 0; i < k; i++)
            {
                List<int> x = new List<int>();
                List<int> y = new List<int>();
                foreach (Point p in centroid_list)
                {
                    x.Add(p.X);
                    y.Add(p.Y);
                }
                chart2.Series.Add("centroid " + (i + 1).ToString());
                chart2.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                chart2.Series[i].IsVisibleInLegend = false;
                chart2.Series[i].Points.DataBindXY(x, y);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;                                  
            while (clasters.refreshCentroids())
            {
               clasters.count();
                        
                for (int i = 0; i<clasters_arr.Length; i++)
                {
                    clasters_arr[i] = new List<Point>(clasters.Clasters[i]);                   
                }
                centroid_list = new List<Point>(clasters.Centroids);              
                worker.ReportProgress(1);
                //Thread.Sleep(300);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            createChart1();
            createChart2();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            label3.Text = "Finished";
        }
    }
}
