﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clasters
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
        }
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    class ClasterAnalyzer
    {
        private List<Point> points = new List<Point>();
        private Point[] centroids;
        private List<Point>[] clasters;
        internal List<Point> Points { get => points; set => points = value; }
        internal List<Point>[] Clasters { get => clasters; set => clasters = value; }
        internal Point[] Centroids { get => centroids; set => centroids = value; }

        private int k;
        private Random random = new Random();

       

        public ClasterAnalyzer(String filename,int k)
        {
            using (StreamReader reader = File.OpenText(filename))
            {
                do
                {
                    string s = reader.ReadLine();
                    string[] mys = s.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    Point p = new Point();
                    p.X = int.Parse(mys[0]);
                    p.Y = int.Parse(mys[1]);
                    Points.Add(p);
                } while (!reader.EndOfStream);
            }
            this.k = k;
            prepare();
        }

        /************************************************COUNT***************************************************/
        public void count()
        {
            lock (this)
            {
                clearClasters();         
                foreach (Point p in points)
                {
                    int index = 0;
                    double min = 9999;
                    for(int j = 0; j<centroids.Length; j++)
                    {
                        Point centr = centroids[j];
                        double dist = Math.Sqrt(Math.Pow((p.X - centr.X),2) + Math.Pow((p.Y - centr.Y), 2));
                        if (dist<min)
                        {
                            min = dist;
                            index = j;
                        }                  
                    }
                    clasters[index].Add(p);
                }
            }
        }

        private void clearClasters()
        {
            for (int i = 0; i<clasters.Length; i++) {
                clasters[i].Clear();
            }
        }

        /************************************************CENTROID***************************************************/
        public bool refreshCentroids()
        {
            lock (this)
            {
                bool answer = false;
                for (int i = 0; i < clasters.Length; i++)
                {
                    Point centr = new Point();
                    foreach (Point p in clasters[i])
                    {
                        centr.X += p.X;
                        centr.Y += p.Y;
                    }
                    int total = clasters[i].Count;
                    if (!(total == 0))
                    {
                        centr.X = centr.X / total;
                        centr.Y = centr.Y / total;
                    }
                    if (!(centr.X == Centroids[i].X && centr.Y == Centroids[i].Y))
                    {
                        answer = true;
                        Centroids[i] = centr;
                    }
                }
                return answer;
            }
        }

        /************************************************OTHERS***************************************************/

        private void prepare()
        {
            int xMax = getMax("X");
            int yMax = getMax("Y");
            int x;
            int y;
            centroids = new Point[k];
            for (int i = 0; i < centroids.Length; i++)
            {
                x = (int)(random.NextDouble() * xMax);
                y = (int)(random.NextDouble() * yMax);
                Point p = new Point(x,y);
                centroids[i] = p;               
            }           
            clasters = new List<Point>[k];
            for (int i = 0; i<clasters.Length; i++)
            {
                clasters[i] = new List<Point>();
            }
        }

        private int getMax(string s)
        {
            int max = 0;
                foreach (Point p in Points)
                {
                    if (s.Equals("X"))
                    {
                        if (p.X > max)
                            max = p.X;
                    }
                    else
                    {
                        if (p.Y > max)
                            max = p.Y;
                    }
                }
            return max;
        }           
    }
}
